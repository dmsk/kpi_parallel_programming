package secondlab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

public class SecondLabTransformer extends RecursiveTask<List<String>> {
    private List elements;
    private final int THRESHOLD;
    private SecondLab secondLab;

    public SecondLabTransformer(List elements, SecondLab secondLab, int threshold) {
        this.elements = elements;
        THRESHOLD = threshold;
        this.secondLab = secondLab;
    }

    @Override
    protected List<String> compute() {
        if (elements.size() > THRESHOLD) {
            List<String> result = new ArrayList<>();
            for (SecondLabTransformer secondLabExecutor : ForkJoinTask.invokeAll(createSubtasks())) {
                result.addAll(secondLabExecutor.join());
            }
            return result;
        }
        return processing(elements);
    }

    private Collection<SecondLabTransformer> createSubtasks() {
        List<SecondLabTransformer> dividedTasks = new ArrayList<>();
        dividedTasks.add(new SecondLabTransformer(
                Arrays.asList(Arrays.copyOfRange(elements.toArray(), 0, elements.size() / 2)), secondLab, THRESHOLD));
        dividedTasks.add(new SecondLabTransformer(
                Arrays.asList(Arrays.copyOfRange(elements.toArray(), elements.size() / 2, elements.size())), secondLab, THRESHOLD));
        return dividedTasks;
    }

    private List<String> processing(List<String> currentElements) {
        return currentElements
                .stream()
                .map(element -> secondLab.transformFunction(element))
                .collect(Collectors.toList());
    }
}
