package secondlab;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BenchMarkExecutor {

    public void testExecution(String fileName, int threadsAmount) {
        long startTime = System.currentTimeMillis();
        List<String> initialElements;
        Path targetPath = Paths.get(fileName);
        try (Stream<String> lines = Files.lines(targetPath)) {
            initialElements = lines.collect(Collectors.toList());
        } catch (IOException e) {
            initialElements = new ArrayList<>();
        }

        SecondLab secondLab = new SecondLab();

        // Reverse Strings
        SecondLabTransformer secondLabExecutor = new SecondLabTransformer(initialElements, secondLab, initialElements.size()/threadsAmount);
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.execute(secondLabExecutor);
        List<String> reversedStrings = secondLabExecutor.join();

        // Transform String to Object with index
        List<Pair> stringPairs = secondLab.mapToPairs(reversedStrings);

        // Calculate element with minimal length
        SecondLabCalculator secondLabCalculator = new SecondLabCalculator(stringPairs, secondLab, stringPairs.size()/threadsAmount);
        forkJoinPool.execute(secondLabCalculator);
        Pair resultPair = secondLabCalculator.join();
       // System.out.println("Result: "+resultPair);
        long endTime = System.currentTimeMillis();
        System.out.println("Total execution time for "+threadsAmount+" threads: " + (endTime - startTime) + "ms");
    }
}
