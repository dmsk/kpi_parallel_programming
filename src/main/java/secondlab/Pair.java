package secondlab;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Pair<I, E> {
    private final I index;
    private final E element;
}
