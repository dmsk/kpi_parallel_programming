package secondlab;

public class Runner {
    public static void main(String[] args) {
        BenchMarkExecutor benchMarkExecutor = new BenchMarkExecutor();

        // hot start for future threads
        benchMarkExecutor.testExecution("src/main/resources/SecondLab/input_8500.txt", 20);

        for (int i = 5; i <= 20; i+=5) {
            benchMarkExecutor.testExecution("src/main/resources/SecondLab/input_8500.txt", 5);
            benchMarkExecutor.testExecution("src/main/resources/SecondLab/input_4000.txt", 10);
            benchMarkExecutor.testExecution("src/main/resources/SecondLab/input_2000.txt", 15);
            benchMarkExecutor.testExecution("src/main/resources/SecondLab/input_1000.txt", 20);
        }
    }
}
