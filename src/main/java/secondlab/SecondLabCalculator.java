package secondlab;

import java.util.*;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class SecondLabCalculator extends RecursiveTask<Pair> {
    private List elements;
    private final int THRESHOLD;
        private SecondLab secondLab;

    public SecondLabCalculator(List elements, SecondLab secondLab, int threshold) {
            this.elements = elements;
            THRESHOLD = threshold;
        this.secondLab = secondLab;
    }

    @Override
    protected Pair compute() {
        if (elements.size() > THRESHOLD) {
            return ForkJoinTask.invokeAll(createSubTasks())
                    .stream()
                    .map(s -> s.join())
                    .min(Comparator.comparing(s -> s.getElement().toString().length())).get();
        }
        return calculate(elements);
    }

    private Collection<SecondLabCalculator> createSubTasks() {
        List<SecondLabCalculator> dividedTasks = new ArrayList<>();
        dividedTasks.add(new SecondLabCalculator(
                Arrays.asList(Arrays.copyOfRange(elements.toArray(), 0, elements.size() / 2)), secondLab, THRESHOLD));
        dividedTasks.add(new SecondLabCalculator(
                Arrays.asList(Arrays.copyOfRange(elements.toArray(), elements.size() / 2, elements.size())), secondLab, THRESHOLD));
        return dividedTasks;
    }

    public Pair calculate(List<Pair> amounts) {
        return amounts
                .stream()
                .min(Comparator.comparing(s -> s.getElement().toString().length())).get();
    }
}
