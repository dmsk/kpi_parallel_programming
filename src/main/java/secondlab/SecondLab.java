package secondlab;

import java.util.ArrayList;
import java.util.List;

public class SecondLab {

    public String transformFunction(String element) {
        int begin = 0;
        int end = element.length() - 1;
        char[] charsElement = element.toCharArray();

        while (end > begin) {
            char temp = charsElement[begin];
            charsElement[begin] = charsElement[end];
            charsElement[end] = temp;
            begin++;
            end--;
        }
        return new String(charsElement);
    }

    public List<Pair> mapToPairs(List<String> elements) {
        List<Pair> stringPair = new ArrayList<>();
        int counter = 0;
        for (String element : elements) {
            stringPair.add(new Pair(counter, element));
            counter++;
        }
        return stringPair;
    }
}
