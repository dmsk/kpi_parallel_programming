package firstlab;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MatrixUtils {

    @SneakyThrows
    public void multiplyMatrixes(int[][] firstMatrix, int[][] secondMatrix) {
        int m = firstMatrix.length;
        int n = secondMatrix[0].length;
        int o = secondMatrix.length;
        int[][] res = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < o; k++) {
                    res[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }
//        for (int i = 0; i < res.length; i++) {
//            for (int j = 0; j < res[0].length; j++) {
//                System.out.format("%6d ", res[i][j]);
//            }
//            System.out.println();
//        }
    }

    public List<int[][]> generateMatrix(int m, int n, int matrixAmount) {
        Random random = new Random();
        List arrays = new ArrayList<int[][]>();
        for (int k = 0; k < matrixAmount; k++) {
            int[][] matrix = new int[m][n];
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = random.nextInt(101) + 1000;
                }
            }
            arrays.add(matrix);
        }
        return arrays;
    }

    public void multiplyMatrixBulk(List<int[][]> matrixes) {
        for (int i = 0; i < matrixes.size()-1; i++) {
            multiplyMatrixes(matrixes.get(i), matrixes.get(i + 1));
        }
    }
}
