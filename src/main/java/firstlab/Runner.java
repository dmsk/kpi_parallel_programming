package firstlab;

public class Runner {
    public static void main(String[] args) {
        CpuMemoryBoundExecutor executor = new CpuMemoryBoundExecutor(10);
        // hot start
        executor.executeCpuBound(8, 1200);
        executor.executeMemoryBound(8, 600);
        for (int i = 2; i <= 8; i+=2) {
            executor.executeCpuBound(i, 1200);
        }
        for (int i = 2; i <= 8; i+=2) {
            executor.executeMemoryBound(i, 600);
        }
        IoBoundExecutor ioBoundExecutor = new IoBoundExecutor(10);
        String directory = "src/main/resources/FirstLab/";
        for (int i = 2; i <= 8; i+=2) {
            ioBoundExecutor.execute(i, directory+String.valueOf(i)+"/");
        }
    }
}
