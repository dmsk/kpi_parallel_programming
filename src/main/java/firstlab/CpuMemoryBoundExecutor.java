package firstlab;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CpuMemoryBoundExecutor {
    private final MatrixUtils matrixUtils;
    private  int threadsAmount;
    private  int matrixAmount;
    private final int CPU_BOUND_MATRIX_SIZE = 1000;
    private final int MEMORY_BOUND_MATRIX_SIZE = 500;

    public CpuMemoryBoundExecutor(int threadsAmount) {
        this.threadsAmount = threadsAmount;
        this.matrixUtils = new MatrixUtils();
    }

    public void executeCpuBound(int threadsAmount, int matrixAmount) {
        this.matrixAmount =matrixAmount;
        this.threadsAmount = threadsAmount;
        long startTime = System.currentTimeMillis();
        execute(CPU_BOUND_MATRIX_SIZE, CPU_BOUND_MATRIX_SIZE);
        long endTime = System.currentTimeMillis();
        System.out.println("Cpu Bound execution time for "+threadsAmount+" threads " + (endTime - startTime) + "ms");
    }

    public void executeMemoryBound(int threadsAmount, int matrixAmount) {
        this.matrixAmount =matrixAmount;
        this.threadsAmount = threadsAmount;
        long startTime = System.currentTimeMillis();
        execute(MEMORY_BOUND_MATRIX_SIZE, MEMORY_BOUND_MATRIX_SIZE);
        long endTime = System.currentTimeMillis();
        System.out.println("Memory Bound execution time for "+threadsAmount+" threads " + (endTime - startTime) + "ms");
    }

    public void execute(int n, int m) {
        List<int[][]> matrixes = matrixUtils.generateMatrix(n, m, matrixAmount);
        int limit = matrixes.size() / threadsAmount;
        splitList(limit, matrixes).forEach(matrixList -> new Thread(() -> matrixUtils.multiplyMatrixBulk(matrixList)).start());
    }

    List<List<int[][]>> splitList(int limit, List<int[][]> matrixes) {
        List<List<int[][]>> splitedList = new ArrayList<>();
        int steps = matrixes.size() / limit;
        for (int i = 0; i < steps; i++) {
            splitedList.add(matrixes.stream()
                    .skip(limit * i)
                    .limit(limit)
                    .collect(Collectors.toList()));
        }
        return splitedList;
    }
}
