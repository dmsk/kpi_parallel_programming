package firstlab;

import lombok.SneakyThrows;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class IoBoundExecutor {
    static int filesize = 129 * 1024;
    byte[] bytes = new byte[filesize];
    private int threadsAmount;

    public IoBoundExecutor(int threadsAmount) {
        this.threadsAmount = threadsAmount;
    }

    @SneakyThrows
    public void execute(int threadsAmount, String directory) {
        this.threadsAmount = threadsAmount;
        Path targetPath = Paths.get(directory);
        if (!Files.exists(targetPath))
            Files.createDirectories(targetPath);

        List<String> fileNames = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            fileNames.add(directory +"input"+ i + ".txt");
        }
        long startTime = System.currentTimeMillis();
        splitList(fileNames, threadsAmount).forEach(fileList -> new Thread(() -> createFiles(fileList)).start());
        long endTime = System.currentTimeMillis();
        System.out.println("Total execution time for "+threadsAmount+" threads: " + (endTime - startTime) + "ms");
    }

    @SneakyThrows
    private void createFiles(List<String> fileNames) {
        for (String fileName : fileNames) {
            File file = new File(fileName);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            Random rand = new Random();
            rand.nextBytes(bytes);
            bufferedOutputStream.write(bytes);
            file.createNewFile();
        }
    }

    private List<List<String>> splitList(List<String> names, int limit) {
        List<List<String>> splitedList = new ArrayList<>();
        int steps = names.size() / limit;
        for (int i = 0; i < steps; i++) {
            splitedList.add(names.stream()
                    .skip(limit * i)
                    .limit(limit)
                    .collect(Collectors.toList()));
        }
        return splitedList;
    }
}
