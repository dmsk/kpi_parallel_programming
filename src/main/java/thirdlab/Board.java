package thirdlab;

import java.util.Date;

public class Board {

    public void synchronizedPaintWithTimeout(int ballsAmount, int size, Date date, double probability) {
        int[][] matrix = new int[size][size];
        matrix[0][0] = ballsAmount;
        moveBallsWithTimeout(matrix, date, probability);
    }

    public void synchronizedPaintWithIterations(int ballsAmount, int size, int iterations, double probability) {
        int[][] matrix = new int[size][size];
        matrix[0][0] = ballsAmount;
        moveBallsWithIterations(matrix, iterations, probability);
    }

    private synchronized void moveBallsWithIterations(int[][] matrix, int iterations, double probability) {
        int counter = 0;
        while (counter < iterations) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    Dot dot = new Dot(i, j);
                    moveBall(dot, matrix.length, probability);
                    matrix[i][j]--;
                    matrix[dot.getX()][dot.getY()]++;
                }
            }
            counter++;
            printMatrix(matrix);
        }
    }

    private synchronized void moveBallsWithTimeout(int[][] matrix, Date date, double probability) {
        long startTime = System.currentTimeMillis();
        while ((System.currentTimeMillis() - startTime) / 1000 < date.getSeconds()) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                    Dot dot = new Dot(i, j);
                    moveBall(dot, matrix.length, probability);
                    matrix[i][j]--;
                    matrix[dot.getX()][dot.getY()]++;
                }
            }
            printMatrix(matrix);
        }
    }

    private void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.println(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private void moveBall(Dot dot, int size, double probability) {
        boolean moveBallUp = true;
        boolean moveBallRight = true;
        int x = dot.getX();
        int y = dot.getY();

        if (Math.random() <= probability && moveBallRight) {
            if (x < size) {
                x++;
            } else {
                moveBallRight = false;
            }
        } else {
            if (x > 0) {
                x--;
            } else {
                moveBallRight = true;
            }
        }

        if (moveBallUp) {
            if (y > 0) {
                y--;
            } else {
                moveBallUp = false;
            }
        } else {
            if (y < size) {
                y++;
            } else {
                moveBallUp = true;
            }
        }
        try {
            Thread.sleep(5);
        } catch (Exception exc) {
        }
    }
}
