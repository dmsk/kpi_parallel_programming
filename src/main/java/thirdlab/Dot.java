package thirdlab;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Dot {
    private int X;
    private int Y;
}
