package thirdlab;

import java.util.Date;

public class Runner {

    public static void main(String[] args) {
        double probability = 0.5;
        int iterationsAmount = 5000;
        int bitsAmount = 100;
        Date date = new Date();
        date.setTime(100);

        Board board = new Board();
        board.synchronizedPaintWithIterations(bitsAmount, 500, iterationsAmount, probability);
        board.synchronizedPaintWithTimeout(bitsAmount, 500, date, probability);
    }
}
